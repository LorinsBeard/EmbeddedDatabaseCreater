#!/usr/bin/python3
# -*- coding: UTF-8 -*-

'''
Created on Aug 5, 2018
File for launch program
@author: MGO
'''

import sys
import argparse
from Logic import CheckFileAndRun

def createParser ():
	parser = argparse.ArgumentParser()
	parser.add_argument ('filePath', nargs='?', default='./DB description.xlsx')
	
	return parser

 
if __name__ == '__main__':
	parser = createParser()
	namespace = parser.parse_args (sys.argv[1:])
	CheckFileAndRun(namespace.filePath)
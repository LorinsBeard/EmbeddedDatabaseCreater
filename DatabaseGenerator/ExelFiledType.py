'''
Created on Aug 4, 2018
File for parsing excel and take necessary data
@author: Maksim Golikov
'''
import os
import xlrd


#Position of cells
NAME_CELL           = 0
TYPE_CELL           = 1
DEFAULT_VAL_CELL    = 2
MIN_VAL_CELL        = 3
MAX_VAL_CELL        = 4
QUONTITY_VAL_CELL   = 5

TYPE_DESCRIBE_NAME_CELL = 0 
TYPE_DESCRIBE_BYTE_CELL = 1


class FiledDB(object):
    '''
    class for describe field of excel and parsing it correctly
    '''
    def __init__(self, name, ftype, defaultValue, minvalue, maxValue, quontity):
        self.name          = name
        self.type          = ftype
        self.default_value = defaultValue
        self.min_value     = minvalue
        self.max_value     = maxValue
        self.quontity      = quontity
        
       

  
    
#return -1 if operation contain same error, else return 
def GetListOfDataFrom(fileName):
        
    if os.path.isfile(fileName):
        DataFromExcel=[]
        excelFile = xlrd.open_workbook(fileName)
        uSheet    = excelFile.sheet_by_index(0)
        exRows    = uSheet.nrows
    
        for row in range(1, exRows):
            f_name     = (str(uSheet.row(row)[NAME_CELL]).replace("text:","")).replace("'","")
            f_type     = str(uSheet.row(row)[TYPE_CELL]).replace("text:","").replace("'","")
            f_default  = str(uSheet.row(row)[DEFAULT_VAL_CELL]).replace("text:","").replace("'","").replace("number:","").replace("empty:u:","")
            f_min      = str(uSheet.row(row)[MIN_VAL_CELL]).replace("text:","").replace("'","").replace("number:","").replace("empty:u:","")
            f_max      = str(uSheet.row(row)[MAX_VAL_CELL]).replace("text:","").replace("'","").replace("number:","").replace("empty:u:","")
            q = str(uSheet.row(row)[QUONTITY_VAL_CELL]).replace("number:","").replace("text:","").replace("'","").replace("empty:u:","")
            if (q != "empty:"):
                f_quontity = str(uSheet.row(row)[QUONTITY_VAL_CELL]).replace("number:","").replace("text:","").replace("'","").replace("empty:u:","")
            else:
                f_quontity = 1
            print( f_name, f_type, f_min, f_max, f_quontity, f_default )
            newObj = FiledDB(f_name, f_type, int(float(f_default)), int(float(f_min)), int(float(f_max)), int(float(f_quontity)) )
            DataFromExcel.append(newObj)
        
        return DataFromExcel
    else:
        print("Exel file is absent.\n")    
        
    
#return -1 if operation contain same error, else return 
def GetListOfTypeDescriptionFrom(fileName):
        
    if os.path.isfile(fileName):
        DataFromExcel={}
        excelFile = xlrd.open_workbook(fileName)
        uSheet    = excelFile.sheet_by_index(1)
        exRows    = uSheet.nrows
    
        for row in range(1, exRows):
            f_name       = str(uSheet.row(row)[TYPE_DESCRIBE_NAME_CELL]).replace("text:","").replace("'","")
            f_byteCount  = str(uSheet.row(row)[TYPE_DESCRIBE_BYTE_CELL]).replace("text:","").replace("'","") .replace("number:","")          
                        
            DataFromExcel[f_name] = int(float(f_byteCount))
        
        return DataFromExcel
    else:
        print("File is absent.\n") 
        
    
'''
Created on Aug 5, 2018
File witch describe logic of program
@author: MGO
'''

#includes
from FileCreator   import CreateFile, WriteDataToFile, AppendDataToFile
from ExelFiledType import GetListOfDataFrom, GetListOfTypeDescriptionFrom
#from FileCreator import *
#from ExelFiledType import *
import os


#Grolbal definition
EXCEL_FILE_NAME    = ""
OUTPUT_DIR_NAME    = "RESULT"
OUTPUT_FILE_1_NAME = "DatabaseDescription.h"
OUTPUT_FILE_2_NAME = "Database.h"    

INCLUDES                 = ["#include <stdint.h>\n","#include \"DatabaseDescription.h\" \n","#include \"CallbackFunction.h\"\n\n\n"]
EXEPTION_MAS             = ["typedef enum{\n", "Initialize_error,\n","ValueParsing_error,\n","ValueType_error,\n","}codeOfError_t;\n\n"]
FIELS_STRUCT_DESCRIPTION = ["typedef struct {\n", "uint16_t        startIndex;\n","dbFielsType_t   dataType;\n","void            (*Callback)();\n","} DBFieldDescription_t; \n\n\n"]
Next_Db_Param_Index      = 0
Data_Storage_Size        = 0
Exist_Types              = ["typedef enum {\n"]
Exist_Param_Names        = ["typedef enum {\n"]
DataBaseMass             = ["static const DBFieldDescription_t DB [] = { \n//  StartIndex       dataType      callback\n"]
DEFAULT_VALUE            = ["static uint8_t DEFAULT [STORAGE_SIZE] = {"];
BAUNDARY_VALUE           = ["static uint8_t BAUNDARY_VALUE [STORAGE_SIZE * 2] = {"];
ReadData                 = []




def CheckFileAndRun(name):
    if os.path.isfile(name):
        print("Start")
        global EXCEL_FILE_NAME
        EXCEL_FILE_NAME = name
        Run()
        print("Finish")
    else:
        print("File absent")    


def Run():
    #Parsing Excel
    global ReadData
    global PossiblseTypes
    global Exist_Param_Names
    global Exist_Types
    global DataBaseMass
    global Data_Storage_Size
    global Next_Db_Param_Index
    
    
    
    ReadData       = GetListOfDataFrom( EXCEL_FILE_NAME )    
    PossiblseTypes = GetListOfTypeDescriptionFrom( EXCEL_FILE_NAME )
    countOfFields  = len(ReadData)
    
    
    Exist_Param_Names.append( ReadData[0].name +",\n" )    
    Exist_Types.append( ReadData[0].type + " = " + str(PossiblseTypes[ReadData[0].type]) + ",\n" )    
    DataBaseMass.append("{ " + str(Next_Db_Param_Index) + ",      " + ReadData[0].type + ",     NULL },\n")    
    Data_Storage_Size += PossiblseTypes[ReadData[0].type] * ReadData[0].quontity    
    addValueToDefaultAndBaundaey(ReadData[0].quontity, 0)
    
    for prm in range(1, countOfFields):
        Exist_Param_Names.append( ReadData[prm].name + ",\n" )
        if not (ReadData[prm].type +" = " +str(PossiblseTypes[ReadData[prm].type]) + ",\n") in Exist_Types :
            Exist_Types.append( ReadData[prm].type +" = " +str(PossiblseTypes[ReadData[prm].type]) + ",\n" )
        
        firs_val    = PossiblseTypes[ReadData[prm - 1].type]
        second_val  = ReadData[prm - 1].quontity
        Next_Db_Param_Index += ( firs_val * second_val )
        
        DataBaseMass.append("{ " +str(Next_Db_Param_Index)+",      "+ ReadData[prm].type +",     NULL },\n")                  
        
        firs_val    = PossiblseTypes[ReadData[prm].type]
        second_val  = ReadData[prm].quontity
        Data_Storage_Size   += ( firs_val * second_val )
    
        addValueToDefaultAndBaundaey(second_val, prm)
                
    
    DataBaseMass.append("};\n\n")   
    Exist_Types.append("}dbFielsType_t;\n\n")
    Exist_Param_Names.append( "prm_Amount\n}dbFielsName_t;\n\n" )
    DEFAULT_VALUE.append("}; \n")
    BAUNDARY_VALUE.append("}; \n")
    
    
    
    
    #Filling files
    CreateFile(OUTPUT_DIR_NAME, OUTPUT_FILE_1_NAME)
    WriteDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_1_NAME),  EXEPTION_MAS)
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_1_NAME),  Exist_Types)
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_1_NAME),  Exist_Param_Names)
    
    
    CreateFile(OUTPUT_DIR_NAME, OUTPUT_FILE_2_NAME)
    WriteDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_2_NAME),  INCLUDES)
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_2_NAME),  ("#define STORAGE_SIZE      "+str(Data_Storage_Size) +"\n\n\n" )  )
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_2_NAME),  FIELS_STRUCT_DESCRIPTION)
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_2_NAME),  DataBaseMass)
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_2_NAME),  "static uint8_t STORAGE [STORAGE_SIZE] = {0};\n" )
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_2_NAME),  DEFAULT_VALUE )
    AppendDataToFile( (OUTPUT_DIR_NAME+"/"+OUTPUT_FILE_2_NAME),  BAUNDARY_VALUE )



def addValueToDefaultAndBaundaey(numberIteration, index):
    while numberIteration > 0:
        val =  ReadData[index].default_value
        global DEFAULT_VALUE
        DEFAULT_VALUE.append( str(val) +"," )
    
        min_ =  ReadData[index].min_value    
        max_ =  ReadData[index].max_value
        global BAUNDARY_VALUE
        BAUNDARY_VALUE.append(str(min_)+","+str(max_)+",")
        
        numberIteration -= 1

